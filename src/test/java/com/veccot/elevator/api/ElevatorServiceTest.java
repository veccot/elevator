package com.veccot.elevator.api;

import org.junit.Test;

import static org.junit.Assert.*;

public class ElevatorServiceTest {
    
    @Test
    public void requestElevator_singleElevator_availableElevatorOnSameFloor() {
        ElevatorService service = new ElevatorService(1, 3);

        Elevator requestedElevator = service.requestElevator(0);
        assertNull(requestedElevator);
    }

    @Test
    public void requestElevator_singleElevator_availableElevatorCanGoToRequestedFloor() {
        ElevatorService service = new ElevatorService(1, 3);

        Elevator requestedElevator = service.requestElevator(1);
        assertNotNull(requestedElevator);
    }

    @Test
    public void requestElevator_singleElevator_alreadyCalledElevator() {
        ElevatorService service = new ElevatorService(1, 3);

        Elevator elevator = service.getElevators().get(0);
        elevator.moveElevator(1);

        Elevator requestedElevator = service.requestElevator(1);
        assertNull(requestedElevator);
    }

    @Test
    public void requestElevator_singleElevator_closestElevator() {
        ElevatorService service = new ElevatorService(1, 3);

        Elevator elevator = service.getElevators().get(0);
        elevator.moveElevator(2);
        elevator.moveUp(2);

        Elevator requestedElevator = service.requestElevator(0);
        assertNotNull(requestedElevator);
    }

    @Test
    public void requestElevator_doubleElevator_closestElevator_sameDistance() {
        ElevatorService service = new ElevatorService(2, 3);

        Elevator requestedElevator = service.requestElevator(1);
        assertNotNull(requestedElevator);
        assertEquals(1, requestedElevator.getId());
    }

    @Test
    public void requestElevator_doubleElevator_closestElevator() {
        ElevatorService service = new ElevatorService(2, 3);

        Elevator elevator = service.getElevators().get(1);
        elevator.moveElevator(3);
        elevator.moveUp(3);

        Elevator requestedElevator = service.requestElevator(2);
        assertNotNull(requestedElevator);
        assertEquals(2, requestedElevator.getId());
    }
    
}