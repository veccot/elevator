package com.veccot.elevator;

import com.google.common.eventbus.EventBus;
import com.veccot.elevator.config.ElevatorApplication;
import com.veccot.elevator.event.ElevatorRequestEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Boiler plate test class to get up and running with a test faster.
 *
 * @author Sven Wesley
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElevatorApplication.class)
public class IntegrationTest {

    @Autowired
    private EventBus eventBus;

    @Autowired
    private ElevatorApplication elevatorApplication;

    @Autowired
    private ThreadPoolExecutor executor;

    private static final int NUMBER_OF_REQUESTS = 50;

    @Test
    public void simulateAnElevatorShaft() throws InterruptedException {

        for (int i = 0; i < NUMBER_OF_REQUESTS; i++) {
            Random random = new Random();
            int nextFloor = random.nextInt(elevatorApplication.getNumberOfFloors());
            eventBus.post(new ElevatorRequestEvent(nextFloor));
        }

        do {
            Thread.sleep(200);
        } while (executor.getActiveCount() > 0);
    }

}
