package com.veccot.elevator.resources;

import com.veccot.elevator.api.ElevatorService;
import com.veccot.elevator.config.ElevatorApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Boiler plate test class to get up and running with a test faster.
 *
 * @author Sven Wesley
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ElevatorApplication.class)
public class ElevatorControllerEndPointsTest {

    @Autowired
    private ElevatorControllerEndPoints endPoints;

    @Autowired
    private ElevatorService elevatorService;

    @Test
    public void ping() {
        assertEquals("pong", endPoints.ping());
    }

    @Test
    public void requestElevator() throws InterruptedException {
        assertFalse(elevatorService.getAvailableElevatorOnFloor(1).isPresent());

        endPoints.requestElevator(1);
        Thread.sleep(300);

        assertTrue(elevatorService.getAvailableElevatorOnFloor(1).isPresent());
    }

    @Test(expected = ValidationException.class)
    public void requestElevator_validateTooLowFloorValue() {
        endPoints.requestElevator(-1);
    }

    @Test(expected = ValidationException.class)
    public void requestElevator_validateTooHighFloorValue() {
        endPoints.requestElevator(9000);
    }

}
