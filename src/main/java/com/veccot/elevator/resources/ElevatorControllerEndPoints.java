package com.veccot.elevator.resources;

import com.google.common.eventbus.EventBus;
import com.veccot.elevator.config.ElevatorApplication;
import com.veccot.elevator.event.ElevatorRequestEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * Rest Resource.
 *
 * @author Sven Wesley
 *
 */
@RestController
@RequestMapping("/rest/v1")
public final class ElevatorControllerEndPoints {

    private final ElevatorApplication elevatorApplication;
    private final EventBus eventBus;

    @Autowired
    public ElevatorControllerEndPoints(ElevatorApplication elevatorApplication, EventBus eventBus) {
        this.elevatorApplication = elevatorApplication;
        this.eventBus = eventBus;
    }

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {

        return "pong";
    }

    @PutMapping(value = "/request/elevator/{floor}")
    public void requestElevator(@NotNull @PathVariable Integer floor){
        validateFloor(floor);
        eventBus.post(new ElevatorRequestEvent(floor));
    }

    private void validateFloor(int floor) {
        int maxFloor = elevatorApplication.getNumberOfFloors();
        if(floor < 0 || floor > maxFloor){
            throw new ValidationException("Requested floor is not valid, it should be between 0 and " + maxFloor);
        }
    }
}
