package com.veccot.elevator.config;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.veccot.elevator.api.ElevatorService;
import com.veccot.elevator.event.ElevatorListener;
import com.veccot.elevator.resources.ElevatorControllerEndPoints;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Preconfigured Spring Application boot class.
 *
 */
@SpringBootApplication
public class ElevatorApplication {

    @Value("${com.veccot.elevator.numberofelevators}")
    private int numberOfElevators;

    @Value("${com.veccot.elevator.numberoffloors}")
    private int numberOfFloors;

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    /**
     * Start method that will be invoked when starting the Spring context.
     *
     * @param args Not in use
     */
    public static void main(final String[] args) {
        SpringApplication.run(ElevatorApplication.class, args);
    }

    /**
     * ThreadPoolExecutor worked better while doing integration testing, instead of default Executor.
     *
     * @return ThreadPoolExecutor thread pool
     */
    @Bean(destroyMethod = "shutdown")
    public ThreadPoolExecutor elevatorPool() {
        return new ThreadPoolExecutor(numberOfElevators, numberOfElevators,
                10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(numberOfElevators));
    }

    /**
     * Create an event bus with registered event listener.
     *
     * @return EventBus for async task execution
     */
    @Bean
    public EventBus eventBus(ElevatorListener elevatorListener) {
        EventBus asyncEventBus = new AsyncEventBus(Executors.newCachedThreadPool());
        asyncEventBus.register(elevatorListener);
        return asyncEventBus;
    }

    @Bean
    public ElevatorService elevatorService(){
        return new ElevatorService(numberOfElevators, numberOfFloors);
    }

    @Bean
    public ElevatorListener eventListener(ElevatorService controller, ThreadPoolExecutor executor) {
        return new ElevatorListener(controller, executor);
    }

    @Bean
    public ElevatorControllerEndPoints elevatorControllerEndPoints(ElevatorApplication elevatorApplication, EventBus eventBus){
        return new ElevatorControllerEndPoints(elevatorApplication, eventBus);
    }
}
