package com.veccot.elevator.event;

import com.google.common.eventbus.Subscribe;
import com.veccot.elevator.api.ElevatorImpl;
import com.veccot.elevator.api.ElevatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadPoolExecutor;

@Service
public class ElevatorListener {

    private static final Logger LOG = LoggerFactory.getLogger(ElevatorListener.class);

    private final ElevatorService elevatorService;
    private final ThreadPoolExecutor executor;

    @Autowired
    public ElevatorListener(ElevatorService elevatorService, ThreadPoolExecutor executor) {
        this.elevatorService = elevatorService;
        this.executor = executor;
    }

    @Subscribe
    public void requestElevator(ElevatorRequestEvent event) {
        LOG.info("New elevator request for floor:{}", event.getFloor());
        ElevatorImpl elevator = (ElevatorImpl) elevatorService.requestElevator(event.getFloor());
        if (elevator != null && !elevator.isBusy()) {
            LOG.info("Elevator id:{} is heading for floor:{}", elevator.getId(), event.getFloor());
            elevator.moveElevator(event.getFloor());
            executor.execute(elevator);
        }
    }
}
