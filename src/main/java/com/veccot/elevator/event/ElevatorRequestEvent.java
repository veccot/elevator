package com.veccot.elevator.event;

public class ElevatorRequestEvent {
    private final int floor;

    public ElevatorRequestEvent(int floor) {
        this.floor = floor;
    }

    public int getFloor() {
        return floor;
    }
}
