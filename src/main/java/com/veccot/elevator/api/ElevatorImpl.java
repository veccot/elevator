package com.veccot.elevator.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElevatorImpl implements Elevator, Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ElevatorImpl.class);

    private static final int MOVING_TO_FLOOR_DELAY = 100;

    private final int id;
    private final int maxFloor;
    private int currentFloor;
    private int addressedFloor;

    public ElevatorImpl(int id, int maxFloor) {
        this.id = id;
        this.maxFloor = maxFloor;
        this.currentFloor = 0;
        this.addressedFloor = 0;
    }

    @Override
    public Direction getDirection() {
        if(addressedFloor > currentFloor){
            return Direction.UP;
        } else if(addressedFloor < currentFloor){
            return Direction.DOWN;
        }
        return Direction.NONE;
    }

    @Override
    public int getAddressedFloor() {
        return addressedFloor;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void moveElevator(int toFloor) {
        this.addressedFloor = toFloor;
    }

    @Override
    public void moveUp(int floors) {
        if (currentFloor < maxFloor) {
            currentFloor += floors;
        }
    }

    @Override
    public void moveDown(int floors) {
        if (currentFloor > 0) {
            currentFloor -= floors;
        }
    }

    @Override
    public boolean isBusy() {
        return currentFloor != addressedFloor;
    }

    @Override
    public int currentFloor() {
        return currentFloor;
    }

    @Override
    public void run() {
        LOG.info("Elevator id:{} is at floor:{} heading for floor:{}", getId(), currentFloor(), getAddressedFloor());

        while (isBusy()){
            movingToFloorDelay();
            switch (getDirection()){
                case UP:
                    moveUp(1);
                    break;
                case DOWN:
                    moveDown(1);
                    break;
            }
            LOG.info("Elevator id:{} is at floor:{} heading for floor:{}" + shouldDing(), getId(), currentFloor(), getAddressedFloor());
        }
    }

    private void movingToFloorDelay() {
        try {
            Thread.sleep(MOVING_TO_FLOOR_DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String shouldDing() {
        return isBusy() ? "" : " - DING!";
    }

    @Override
    public String toString() {
        return "ElevatorImpl{" +
                "id=" + id +
                ", direction=" + getDirection() +
                ", currentFloor=" + currentFloor +
                ", addressedFloor=" + addressedFloor +
                ", isBusy=" + isBusy() +
                '}';
    }
}
