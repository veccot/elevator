package com.veccot.elevator.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

@Service
public class ElevatorService implements ElevatorController {

    private static final Logger LOG = LoggerFactory.getLogger(ElevatorService.class);

    private static final int WAIT_FOR_AVAILABLE_ELEVATORS_DELAY = 100;

    private final List<Elevator> elevators = Collections.synchronizedList(new ArrayList<>());

    @SuppressWarnings("SpringJavaAutowiringInspection")
    public ElevatorService(int numberOfElevators, int numberOfFloors) {
        IntStream.range(1, numberOfElevators + 1).forEach(id -> elevators.add(new ElevatorImpl(id, numberOfFloors)));
    }

    @Override
    public Elevator requestElevator(int toFloor) {
        Optional<Elevator> calledElevator = getAvailableOrAlreadyCalledElevator(toFloor);
        if(calledElevator.isPresent()){
            LOG.info("Request to floor:{} is already handled by elevator id:{}", toFloor, calledElevator.get().getId());
            return null;
        }

        List<Elevator> availableElevators = getAvailableElevators();
        while (availableElevators.isEmpty()){
            LOG.info("Waiting for next available elevator going to floor:{}", toFloor);
            waitForAvailableElevator();
            availableElevators = getAvailableElevators();
        }

        Optional<Elevator> closestElevator = getClosestElevator(toFloor, availableElevators);
        if(closestElevator.isPresent()){
            LOG.info("Request to floor:{} is handled by closest elevator id:{}", toFloor, closestElevator.get().getId());
            return closestElevator.get();
        }
        LOG.warn("No elevator could handle request to floor:{}", toFloor);
        return null;
    }

    private Optional<Elevator> getAvailableOrAlreadyCalledElevator(int toFloor) {
        return getElevators().stream()
                .filter(elevator ->
                        (!elevator.isBusy() && elevator.currentFloor() == toFloor) ||
                        (elevator.isBusy() && elevator.getAddressedFloor() == toFloor))
                .findFirst();
    }

    private List<Elevator> getAvailableElevators() {
        return getElevators().stream().filter(elevator -> !elevator.isBusy()).collect(toList());
    }

    private void waitForAvailableElevator() {
        try {
            Thread.sleep(WAIT_FOR_AVAILABLE_ELEVATORS_DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Optional<Elevator> getClosestElevator(int toFloor, List<Elevator> availableElevators) {
        return availableElevators.stream()
                .filter(elevator -> !elevator.isBusy())
                .min(Comparator.comparingInt(e -> Math.abs(e.currentFloor() - toFloor)));
    }

    @Override
    public List<Elevator> getElevators() {
        return elevators;
    }

    public Optional<Integer> getAvailableElevatorOnFloor(int floor){
        return getElevators().stream()
                .filter(elevator ->
                        !elevator.isBusy() && elevator.currentFloor() == floor)
                .map(Elevator::getId)
                .findFirst();
    }

    @Override
    public void releaseElevator(Elevator elevator) {
        elevator.moveElevator(elevator.currentFloor());
    }
}
