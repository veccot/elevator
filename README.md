# Elevator

Simulate a number of elevators moving between floors

## Build And Run

Build and run the code with Maven

    mvn package
    mvn spring-boot:run

or start the target JAR file 

    mvn package
    java -jar target/elevator-1.0-SNAPSHOT.jar